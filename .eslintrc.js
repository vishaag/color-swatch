module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: ["eslint:recommended", "plugin:react/recommended"],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: "module",
  },
  plugins: ["react"],
  ignorePatterns: ["dist", "public", "node_modules"],
  rules: {},
  settings: {
    react: {
      version: "detect",
    },
  },
};
