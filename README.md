
✨ Color Swatch Toolkit ✨

A react-based toolkit that can render an array of five colors with the ability to convert between color spaces.


Live Production Link : [https://color-swatch.vercel.app/](https://color-swatch.vercel.app/)


## Getting Started

First, install all the npm packages using —
```bash
npm install
```

To start the development server, run the following command —
```bash
npm run start
```


Open [http://localhost:1234](http://localhost:1234) with your browser to see the applications running.

To just build the files, run the following command —
```bash
npm run build

```

To lint js & css files, run the following command (make sure to run this after making any changes)  —
```bash
npm run lint
```

To format the code using prettier, run the following command —
```bash
npm run format
```

## Design Considerations
#### Tech stack / Packages

- [React.js](https://reactjs.org/) (React.js) for the frontend
- [color-convert](https://www.npmjs.com/package/color-convert) for converting between color spaces
- [Parcel](https://parceljs.org/) For bundling
- [ESLint](https://eslint.org/) For javascript linting
- [Stylelint](stylelint.io) for CSS linting
- [Prettier](https://prettier.io/) for making the code look pretty
- [Vercel](https://vercel.com/) for hosting

## Folder Structure

```

color-swatch
│   components
│   styles
│
└──utils
     │ constant.js
     │ convertColorSpace.js

```

## How to add new color spaces

Out of the box, the toolkit supports conversion between RGB and HSL color spaces. But it's easy to add new color spaces if they're needed. 

If you'd rather watch a quick 3 minute video, watch this video here -> [https://www.loom.com/share/67a38a42c330420aac70524238660add](https://www.loom.com/share/67a38a42c330420aac70524238660add) 

1. Add the new color space in the `enabledColorSpaces` array in `contants.js`

    For example, to add CMYK, add "cmyk" to the `enabledColorSpaces` array. This will make it available in the dropdown.

    ```javascript
        const enabledColorSpaces = ["rgb", "hsl", "cmyk"];

        export { enabledColorSpaces };

    ```  
    Now the logic to convert between RGB/HSL to CMYK needs to be added.


2. Open `utils/convertColorSpace.js`. Here, we can add the condition and logic to convert to CMYK  

    ```javascript
    // RGB to Other Color Spaces
      ...
      if (from === "rgb" && to === "cmyk") {
        return convert.rgb.cmyk(
          colorComponents.red,
          colorComponents.green,
          colorComponents.blue
        );
      }

      // HSL to Other Color Spaces
      ...
      if (from === "hsl" && to === "cmyk") {
        return convert.hsl.cmyk(
          colorComponents.hue,
          colorComponents.saturation,
          colorComponents.lightness
        );
      }
    ``` 
    Logic for CMYK to other color spaces is not needed as the API only returns in RGB and HSL values.  
    
    Note:  
        1. For color conversion, the [color-convert](https://www.npmjs.com/package/color-convert) package is used. So whatever conversions the packages supports can be easily added in the same way.
        If a color space that isn't supported needs to be added, a custom algorithm or any other package can be used appropriately — just make sure to remember to add the logic here.  
        2. Currently, the API only returns RGB and HSL values. Incase the API returns colors in other color spaces as well, the corrsponding logic must be added. For example, if the API returns in `hsv`         formats as well, conditions for `if (from === "hsv" && to === "rgb")`, `if (from === "hsv" && to === "hsl")` & `if (from === "hsv" && to === "cmyk")` must be added as well.

3. Make a PR with these changes (add @vishaag as reviewer). After the merge, the changes will automatically be deployed on [https://color-swatch.vercel.app/](https://color-swatch.vercel.app/) after Vercel deploys the new build (about 30 seconds).

## Changelog
### 1.0
- Fetches colors from https://challenge.structrs.com and renders them
- Ability to switch between RGB and HSL color spaces
- Allows easy extensibility to other color spaces
- Responsive design




