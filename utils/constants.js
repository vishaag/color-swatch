// Enabled color spaces — Add color space here to make it available in the dropdown
const enabledColorSpaces = ["rgb", "hsl"];

export { enabledColorSpaces };
