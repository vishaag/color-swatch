const convert = require("color-convert");

const convertColorSpace = ({ colorComponents, from, to }) => {
  if (from === to) return Object.values(colorComponents);

  // RGB to Other Color Spaces
  if (from === "rgb" && to === "hsl") {
    return convert.rgb.hsl(
      colorComponents.red,
      colorComponents.green,
      colorComponents.blue
    );
  }

  if (from === "rgb" && to === "hex") {
    return convert.rgb.hex(
      colorComponents.red,
      colorComponents.green,
      colorComponents.blue
    );
  }

  // HSL to Other Color Spaces
  if (from === "hsl" && to === "rgb") {
    return convert.hsl.rgb(
      colorComponents.hue,
      colorComponents.saturation,
      colorComponents.lightness
    );
  }
  if (from === "hsl" && to === "hex") {
    return convert.hsl.hex(
      colorComponents.hue,
      colorComponents.saturation,
      colorComponents.lightness
    );
  }

  return null;
};

export default convertColorSpace;
