import React from "react";
import ColorSwatches from "./components/ColorSwatches";

export function App() {
  return (
    <div className="container">
      <header>
        <h1 className="title">Color Swatch</h1>
      </header>
      <section className="main-section">
        <ColorSwatches />
      </section>
      <footer className="footer">
        <a
          href="https://vishaag.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Submitted by Vishaag
        </a>
      </footer>
    </div>
  );
}
