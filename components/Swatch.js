import React from "react";

export default function Swatch({ background, children }) {
  return (
    <div className="swatch" style={{ background: background }}>
      <div className="swatch-child">{children}</div>
    </div>
  );
}
