import React, { useEffect, useState, useRef } from "react";
import Swatch from "./Swatch";
import convertColorSpace from "../utils/convertColorSpace";
import { enabledColorSpaces } from "../utils/constants";

export default function ColorSwatches() {
  const [formattedColors, setFormattedColors] = useState([]);
  const fetchedColors = useRef([]);
  const [chosenColorSpace, setChosenColorSpace] = useState("rgb");

  const handleChangeColorSpace = (event) => {
    setChosenColorSpace(event.target.value);
  };

  const formatFetchedColors = (colorsArray) => {
    const formattedColorsArray = colorsArray.map((color) => {
      return {
        convertedColorSpace: convertColorSpace({
          colorComponents: color.components,
          from: color.kind,
          to: chosenColorSpace,
        }),
        hex: convertColorSpace({
          colorComponents: color.components,
          from: color.kind,
          to: "hex",
        }),
      };
    });
    return formattedColorsArray;
  };

  const getNewColors = async () => {
    try {
      const response = await fetch(
        "https://challenge.structrs.com/rest/colors/list"
      );
      const newFetchedColors = await response.json();
      fetchedColors.current = newFetchedColors;
      const newFormattedColors = formatFetchedColors(newFetchedColors);
      setFormattedColors(newFormattedColors);
    } catch (error) {
      console.log("error", error);
    }
  };

  useEffect(() => {
    getNewColors();
  }, []);

  useEffect(() => {
    const convertedColors = formatFetchedColors(fetchedColors.current);
    setFormattedColors(convertedColors);
  }, [chosenColorSpace]);

  return (
    <main className="color-swatches">
      <div className="options">
        <div className="color-space">
          <label htmlFor="groupBy">Select Color Space</label>
          <select name="groupBy" id="groupBy" onChange={handleChangeColorSpace}>
            {enabledColorSpaces.map((colorSpace) => {
              return (
                <option value={colorSpace} key={colorSpace}>
                  {colorSpace.toUpperCase()}
                </option>
              );
            })}
          </select>
        </div>
        <button className="refresh-btn" id="refresh-btn" onClick={getNewColors}>
          Refresh 🔄
        </button>
      </div>
      <div className="swatches">
        {formattedColors.map((color) => {
          return (
            <Swatch background={`#${color.hex}`} key={color.hex}>
              {`${chosenColorSpace} — ${JSON.stringify(
                color.convertedColorSpace
              )}`}
            </Swatch>
          );
        })}
      </div>
    </main>
  );
}
